# CompareIn

A [Sublime Text 3](http://www.sublimetext.com/) package providing a standardized interface for calling a user-defined comparison/merge tool.

## Features

*   Standardized interface for calling a user-defined comparison/merge tool.
*   Define multiple comparison/merge tools. The first tool that works will be called.
*   No need to write your own interface to a comparison/merge tool, just call the compare_in command.

## Packages Integrating with CompareIn
*   [Incremental Backup](https://bitbucket.org/kbaskett/incrementalbackup)

# Installation

## Package Control

Install [Package Control](http://wbond.net/sublime_packages/package_control). Add this repository (https://bitbucket.org/kbaskett/comparein) to Package Control. CompareIn will show up in the packages list.

## Manual installation

Go to the "Packages" directory (`Preferences` > `Browse Packages…`). Then download or clone this repository:

https://bitbucket.org/kbaskett/comparein.git


# Options

When editing settings, always make the changes to your user settings file (`Preferences` > `Package Settings` > `CompareIn` > `Settings - User`). Any changes to the package settings file will be overwritten when the package is updated.

*    *compare_tools*: an ordered list of programs to use for comparison. Each tool consists of the following parameters.
    *   *tool*: the path to the tool or Sublimerge. If specifying the path to a tool, you should include the following items that will be replaced when the command is run. (Note that if you use Sublimerge as your tool, no additional configuration is needed.)
        1. `{left_path}` - replaced by the path to the file appearing on the left
        2. `{right_path}` - replaced by the path to the file appearing on the right
        3. `{other_arguments}` - replaced by any additional arguments specified
    *   *left_read_only*: the command line option to make the left file read-only. Optional.
    *   *right_read_only*: the command line option to make the right file read-only. Optional.
    *   *left_title*: the command line option to specify a title for the left file.
    *   *right_title*: the command line option to specify a title for the right file.

## Example settings file

```javascript
{
    "compare_tools": [
        {   // Sublimerge
            "tool": "Sublimerge",
        },

        {   // Beyond Compare
            // Windows
            "tool": "BCompare.exe {other_arguments} \"{left_path}\" \"{right_path}\"",
            "left_read_only": "/lro",
            "right_read_only": "/rro",
            "left_title": "/lefttitle=\"{left_title}\"",
            "right_title": "/righttitle=\"{right_title}\""
        },

        {   // Beyond Compare
            // Linux
            "tool": "bcompare {other_arguments} \"{left_path}\" \"{right_path}\"",
            "left_read_only": "-lro",
            "right_read_only": "-rro",
            "left_title": "-lefttitle=\"{left_title}\"",
            "right_title": "-righttitle=\"{right_title}\"",
        },
    ]
}
```

