import os
import subprocess
import sys
import tempfile

import sublime
import sublime_plugin

try:
    import sublimelogging
    logger = sublimelogging.getLogger(__name__)
except ImportError:
    import logging
    logger = logging.getLogger(__name__)

SublimergeInstalled = False
Settings = None
COMPARE_IN_INSTALLED_CONTEXT_KEY = "compare_in_installed"


def plugin_loaded():
    global SublimergeInstalled, Settings
    SublimergeInstalled = 'sublimerge' in sys.modules.keys()
    Settings = sublime.load_settings('CompareIn.sublime-settings')


def get_view_contents(view):
    return view.substr(sublime.Region(0, view.size()))


def make_temp_file(contents, suffix=''):
    with tempfile.NamedTemporaryFile(
            mode='w', suffix=suffix, delete=False) as temp:
        temp.write(contents)
        return temp.name


class CompareInListenerCommand(sublime_plugin.EventListener):

    def on_query_context(self, view, key, operator, operand, match_all):
        if key != COMPARE_IN_INSTALLED_CONTEXT_KEY:
            return
        elif not isinstance(operand, bool):
            return
        elif operator == sublime.OP_EQUAL:
            return operand
        elif operator == sublime.OP_NOT_EQUAL:
            return not operand
        else:
            return


class CompareInCommand(sublime_plugin.ApplicationCommand):

    def run(self,
            left_file=None, right_file=None,
            left_view=None, right_view=None,
            left_contents=None, right_contents=None,
            left_read_only=False, right_read_only=False,
            left_title=None, right_title=None):

        left_side = ComparisonSide(left_file, left_view, left_contents,
                                   left_read_only, left_title)
        right_side = ComparisonSide(right_file, right_view, right_contents,
                                    right_read_only, right_title)

        if not left_side.file_path:
            logger.error('Invalid left side')
            return
        elif not right_side.file_path:
            logger.error('Invalid right path')
            left_side.delete_temp()
            return

        logger.info('Left Side = %s', left_side)
        logger.info('Right Side = %s', right_side)

        tools = Settings.get('compare_tools', list())
        for t in tools:
            try:
                if self.try_compare(t, left_side, right_side):
                    break
            except InvalidSideError as e:
                if e.side is left_side:
                    logger.error('Invalid left path: %s', e.side)
                else:
                    logger.error('Invalid right path: %s', e.side)
                left_side.delete_temp()
                right_side.delete_temp()
                raise e

    def try_compare(self, tool_settings, left_side, right_side):
        tool = tool_settings['tool']
        if (tool.lower() == 'sublimerge') and SublimergeInstalled:
            return self.compare_sublimerge(left_side, right_side)
        else:
            return self.compare_external(tool_settings, left_side, right_side)

    def compare_sublimerge(self, left_side, right_side):
        arguments = {'left': left_side.file_path,
                     'right': right_side.file_path,
                     'left_read_only': left_side.read_only,
                     'right_read_only': right_side.read_only}
        if left_side.title:
            arguments['left_title'] = left_side.title
        if right_side.title:
            arguments['right_title'] = right_side.title
        sublime.active_window().run_command('sublimerge_diff', arguments)
        return True

    def compare_external(self, tool_settings, left_side, right_side):
        tool = tool_settings['tool']
        arguments = {'left_path': left_side.file_path,
                     'right_path': right_side.file_path,
                     'left_title': left_side.title,
                     'right_title': right_side.title}
        other_arguments = ''
        if left_side.has_other_arguments or right_side.has_other_arguments:
            if '{other_arguments}' not in tool:
                tool += '{other_arguments}'

            if left_side.read_only:
                try:
                    left_read_only = tool_settings['left_read_only']
                except KeyError:
                    pass
                else:
                    left_read_only = left_read_only.format(**arguments)
                    other_arguments += (' ' + left_read_only)

            if right_side.read_only:
                try:
                    right_read_only = tool_settings['right_read_only']
                except KeyError:
                    pass
                else:
                    right_read_only = right_read_only.format(**arguments)
                    other_arguments += (' ' + right_read_only)

            if left_side.title:
                try:
                    left_title = tool_settings['left_title']
                except KeyError:
                    pass
                else:
                    left_title = left_title.format(**arguments)
                    other_arguments += (' ' + left_title)

            if right_side.title:
                try:
                    right_title = tool_settings['right_title']
                except KeyError:
                    pass
                else:
                    right_title = right_title.format(**arguments)
                    other_arguments += (' ' + right_title)

        arguments['other_arguments'] = other_arguments
        tool = tool.format(**arguments)
        logger.debug('tool = %s', tool)

        try:
            subprocess.Popen(tool)
        except OSError:
            return False
        else:
            return True


class ComparisonSide(object):
    """docstring for ComparisonSide"""

    ST_FILE = "File"
    ST_VIEW = "View"
    ST_CONTENTS = "Contents"

    # The file_name property overrides the other two types
    @property
    def file_name(self):
        return self._file_name

    @file_name.setter
    def file_name(self, value):
        self._file_name = value
        self.side_type = ComparisonSide.ST_FILE
        self._view = None
        self._contents = None

    # The view property is overridden by file_name and overrides contents
    @property
    def view(self):
        return self._view

    @view.setter
    def view(self, value):
        try:
            if self._file_name is not None:
                return
        except AttributeError:
            self._file_name = None
        self._view = value
        self.side_type = ComparisonSide.ST_VIEW
        self._contents = None

    # The contents property is overridden by the other two types
    @property
    def contents(self):
        return self._contents

    @contents.setter
    def contents(self, value):
        try:
            if self._file_name is not None:
                return
        except AttributeError:
            self._file_name = None
        try:
            if self._view is not None:
                return
        except AttributeError:
            self._view = None
        self._contents = value
        self.side_type = ComparisonSide.ST_CONTENTS

    @property
    def file_path(self):
        try:
            return self._file_path
        except AttributeError:
            pass

        try:
            if self.file_name is not None:
                self._file_path = self.file_name
                return self._file_path
        except AttributeError:
            self.file_name = None
            raise InvalidSideError(self)

        if self.view is not None:
            view_file_name = self.view.file_name()
            if not view_file_name:
                contents = get_view_contents(self.view)
                if view_file_name:
                    suffix = os.path.splitext(view_file_name)[1]
                else:
                    suffix = ''
                self._file_path = self.temp_file = make_temp_file(
                    contents, suffix)
                self.read_only = True
            else:
                self._file_path = view_file_name
            return self._file_path

        if self.contents is not None:
            self._file_path = self.temp_file = make_temp_file(self.contents)
            self.read_only = True
            return self._contents

        raise InvalidSideError(self)

    @property
    def has_other_arguments(self):
        if self.read_only or self.title:
            return True
        else:
            return False

    def __init__(self, file_name=None, view=None, contents=None,
                 view_only=False, title=None):
        super(ComparisonSide, self).__init__()
        self.file_name = file_name
        self.view = view
        self.contents = contents
        self.temp_file = None
        self.read_only = view_only
        self.title = title

    def __str__(self):
        if self.side_type == ComparisonSide.ST_FILE:
            return 'Side - File: %s' % self.file_name
        elif self.side_type == ComparisonSide.ST_VIEW:
            return 'Side - View: ID = %s' % self.view.id()
        else:
            return 'Side - Contents: %s...' % self.contents[:30]

    def delete_temp(self):
        if self.temp_file:
            if os.path.exists(self.temp_file):
                os.remove(self.temp_file)
                del self._file_path


class InvalidSideError(Exception):
    """docstring for InvalidSideError"""
    def __init__(self, side):
        super(InvalidSideError, self).__init__()
        self.side = side
        self.description = 'Invalid Side - %s' % self.side
